import java.util.Scanner;

public class Kaalculator {

    private static int number;
    private static int firstNumber;
    private static int secondNumber;
    private static int result;
    private static char operation;

    private static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {

        firstNumber = readNumberFromKeyboard();
        secondNumber = readNumberFromKeyboard();

        System.out.println("Введите символ операции ");
        operation = reader.next().charAt(0);

        switch (operation) {
            case '+':
                plus(firstNumber, secondNumber);
                break;
            case '-':
                minus(firstNumber, secondNumber);
                break;
            case '*':
                multiply(firstNumber, secondNumber);
                break;
            case '/':
                break;
            default:
                System.out.println("operation is invalid");
        }

        System.out.println();
    }

    public static void plus(int firstNumber, int secondNumber) {
        int summa = firstNumber + secondNumber;
        System.out.println(summa);
    }

    public static void minus(int firstNumber, int secondNumber) {
        int subtraction = firstNumber - secondNumber;
        System.out.println(subtraction);
        System.out.println("Change text by Edgar");
    }

    public static void multiply(int firstNumber, int secondNumber) {
        int result = firstNumber * secondNumber;
        System.out.println(result);
    }


    static int readNumberFromKeyboard() {
        System.out.println("Введите число ");
        if (reader.hasNextInt()) {
            number = reader.nextInt();
        } else {
            System.out.println("Вы ввели некорректное значение. Введите корректное число");
            reader.next();
            readNumberFromKeyboard();
        }
        return number;
    }

}
